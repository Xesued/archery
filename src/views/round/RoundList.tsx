import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import * as DB from "../../service/db/rounds";
import Box from "@material-ui/core/Box";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

import Avatar from "@material-ui/core/Avatar";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CircularProgress from "@material-ui/core/CircularProgress";
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import auth from "../../service/auth";
import { Round, getRoundScoreForArcher } from "../../model/round";
import AlertDialog from "../../components/AlertDialog";
import SimpleContainer from "../../components/SimpleContainer";

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      marginBottom: theme.spacing(3)
    },
    roundDate: {
      fontSize: ".6rem"
    },
    roundTitle: {
      fontSize: "1.2rem"
    },
    infoBoxBox: {
      width: "45%"
    },
    infoBox: {
      padding: 8,
      textAlign: "center"
    },
    infoHeader: {
      fontSize: ".6rem"
    },
    infoValue: {
      fontSize: "1.4rem"
    },
    avatar: {
      backgroundColor: "#ff4444"
    }
  })
);

function roundHours(round: Round) {
  if (round.started && round.finished) {
    const secs = (round.finished - round.started) / 1000;
    const mins = secs / 60;
    return Math.round(mins);
  }

  return 0;
}

function roundYards(round: Round) {
  return round.targets.reduce((acc, t) => acc + (t.yardage || 0), 0);
}

export default function RoundHistory() {
  const history = useHistory();
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const [rounds, updateRounds] = useState<Round[] | null>(null);
  const classes = useStyle();
  const userId = auth.getUserOrError().uid;
  const [roundOpenMenu, setRoundOpenMenu] = useState<Round | null>(null);
  const [menuAnchorEl, setMenuAnchorEl] = useState<HTMLElement | null>(null);

  const handleMenuClick = (
    event: React.MouseEvent<HTMLElement>,
    round: Round
  ) => {
    setRoundOpenMenu(round);
    setMenuAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setRoundOpenMenu(null);
    setMenuAnchorEl(null);
  };

  const handleDetails = () => {
    setMenuAnchorEl(null);
    if (roundOpenMenu && roundOpenMenu.id) {
      history.push(`/round/${roundOpenMenu.id}`);
    }
  };

  const handleDeleteRequest = () => {
    setMenuAnchorEl(null);
    if (roundOpenMenu && roundOpenMenu.id) {
      setIsDeleteDialogOpen(true);
    }
  };

  const handleDelete = () => {
    setIsDeleteDialogOpen(false);
    if (roundOpenMenu && roundOpenMenu.id) {
      DB.deleteRound(roundOpenMenu); // TODO: Handle error from db
      const newRoundList = rounds?.filter(r => r.id !== roundOpenMenu.id);
      setRoundOpenMenu(null);
      updateRounds(newRoundList || []);
    }
  };

  useEffect(() => {
    DB.getUserRounds(userId).then(rounds => updateRounds(rounds));
  }, [userId]);

  if (!rounds) {
    return <CircularProgress />;
  }

  return (
    <Box>
      <SimpleContainer>
        {rounds.map(round => (
          <Card key={round.id || ""} className={classes.card}>
            <CardHeader
              title={round.name}
              subheader={
                round.started && new Date(round.started).toLocaleDateString()
              }
              avatar={
                <Avatar aria-label="recipe" className={classes.avatar}>
                  R
                </Avatar>
              }
              action={
                <IconButton
                  aria-label="settings"
                  onClick={e => handleMenuClick(e, round)}
                >
                  <MoreVertIcon />
                </IconButton>
              }
            />
            <CardContent>
              <Box display="flex" justifyContent="space-around">
                <Box className={classes.infoBox}>
                  <Box className={classes.infoHeader}>Score</Box>
                  <Box className={classes.infoValue}>
                    {getRoundScoreForArcher(round, userId)}
                  </Box>
                </Box>
                <Box className={classes.infoBox}>
                  <Box className={classes.infoHeader}>Duration</Box>
                  <Box className={classes.infoValue}>{roundHours(round)}m</Box>
                </Box>
                <Box className={classes.infoBox}>
                  <Box className={classes.infoHeader}>Total Yards</Box>
                  <Box className={classes.infoValue}>{roundYards(round)}</Box>
                </Box>
              </Box>
            </CardContent>
          </Card>
        ))}
      </SimpleContainer>
      <Menu
        id="simple-menu"
        anchorEl={menuAnchorEl}
        keepMounted
        open={Boolean(menuAnchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleDetails}>Details</MenuItem>
        <MenuItem onClick={handleDeleteRequest}>Delete</MenuItem>
      </Menu>

      <AlertDialog
        isOpen={isDeleteDialogOpen}
        title="Confirm Delete Target"
        text="You can't undo this action."
        onCancel={() => setIsDeleteDialogOpen(false)}
        onConfirm={handleDelete}
      />
    </Box>
  );
}
