import React, { useState } from "react";

import Box from "@material-ui/core/Box";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import useRound from "../../hooks/useRound";

import TabPanel from "../../components/TabPanel";
import ScreenTitle from "../../components/ScreenTitle";

import ArchersTab from "./detail-tabs/ArchersTab";
import TargetsTab from "./detail-tabs/TargetsTab";
import OverviewTab from "./detail-tabs/OverviewTab";
import { CircularProgress } from "@material-ui/core";

type RoundDetailsProps = {
  match: {
    params: {
      roundId: string;
    };
  };
};

export default function RoundDetails(props: RoundDetailsProps) {
  const [round] = useRound(props.match.params.roundId);
  const [selectedTab, setSelectedTab] = useState(0);
  function handleTabChange(event: any, newValue: number) {
    setSelectedTab(newValue);
  }

  if (!round) {
    return <CircularProgress />;
  }

  return (
    <Box>
      <ScreenTitle title={round.name} />
      <Tabs
        value={selectedTab}
        onChange={handleTabChange}
        variant="fullWidth"
        indicatorColor="secondary"
        aria-label="Details Tabs"
      >
        <Tab label="OVERVIEW" />
        <Tab label="ARCHERS">
          <ArchersTab round={round} />
        </Tab>
        <Tab label="TARGETS">
          <TargetsTab round={round} />
        </Tab>
      </Tabs>
      <TabPanel value={selectedTab} index={0}>
        <OverviewTab round={round} />
      </TabPanel>
      <TabPanel value={selectedTab} index={1}>
        <ArchersTab round={round} />
      </TabPanel>
      <TabPanel value={selectedTab} index={2}>
        <TargetsTab round={round} />{" "}
      </TabPanel>
    </Box>
  );
}
