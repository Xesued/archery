import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

import useAuth from "../../../hooks/useAuth";
import ValueStat from "../../../components/ValueStat";

import {
  Round,
  getRoundAvgYardage,
  getRoundMisses,
  getRoundTargetAvgScore,
  getRoundScoreForArcher
} from "../../../model/round";

type OverviewTabProps = {
  round: Round;
};

const useStyle = makeStyles(theme => ({
  root: {
    flexGrow: 1
  }
}));

export default function OverviewTab(props: OverviewTabProps) {
  const { round } = props;
  const classes = useStyle();
  const user = useAuth();

  const loggedInArcher =
    !(user instanceof Error) && user !== null
      ? round.archers.find(a => a.id === user.uid)
      : null;

  return (
    <Grid className={classes.root} container spacing={3}>
      {loggedInArcher && (
        <Grid item xs={12}>
          <ValueStat
            value={"" + getRoundScoreForArcher(round, loggedInArcher.id)}
            label="My Score"
          />
        </Grid>
      )}
      <Grid item xs={4}>
        <ValueStat value={"" + getRoundAvgYardage(round)} label="Avg Yrd" />
      </Grid>
      <Grid item xs={4}>
        <ValueStat
          value={"" + getRoundTargetAvgScore(round)}
          label="Avg Score"
        />
      </Grid>
      <Grid item xs={4}>
        <ValueStat value={"" + getRoundMisses(round)} label="Misses" />
      </Grid>
    </Grid>
  );
}
