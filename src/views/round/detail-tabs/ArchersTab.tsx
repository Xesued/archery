import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

import { Round, getArcherTargetScores } from "../../../model/round";

type ArchersTabProps = {
  round: Round;
};

const useStyles = makeStyles(theme => ({
  card: {
    marginBottom: theme.spacing(2)
  }
}));

export default function ArchersTab(props: ArchersTabProps) {
  const { round } = props;
  const classes = useStyles();

  return (
    <Box>
      {round.archers.map(archer => {
        const scores = getArcherTargetScores(round, archer);
        const archerMisses = scores.reduce(
          (acc, s) => acc + (s.score === 0 ? 1 : 0),
          0
        );
        const total = scores.reduce((acc, s) => acc + s.score, 0);
        return (
          <Card className={classes.card} key={archer.id}>
            <CardContent>
              <Box display="flex" justifyContent="space-between">
                <Box>{archer.name}</Box>
              </Box>
              <Table size="small">
                <TableBody>
                  <TableRow>
                    <TableCell>Score</TableCell>
                    <TableCell>{total}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Misses</TableCell>
                    <TableCell>{archerMisses}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </CardContent>
          </Card>
        );
      })}
    </Box>
  );
}
