import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { Round } from "../../../model/round";

type TargetsTabProps = {
  round: Round;
};

const useStyles = makeStyles(theme => ({
  card: {
    marginBottom: theme.spacing(2)
  }
}));

export default function TargetsTab(props: TargetsTabProps) {
  const { round } = props;
  const classes = useStyles();

  return (
    <Box>
      {round.targets.map(t => (
        <Card key={t.id} className={classes.card}>
          <CardContent>
            <Box display="flex" justifyContent="space-between">
              <Box>{t.name}</Box>
              <Box>yrd: {t.yardage}</Box>
            </Box>
            <Table size="small">
              <TableBody>
                {round.archers.map(archer => (
                  <TableRow key={archer.id}>
                    <TableCell>{archer.name}</TableCell>
                    <TableCell>
                      {round.targetScores[t.id] &&
                        round.targetScores[t.id].scores[archer.id] &&
                        round.targetScores[t.id].scores[archer.id].score}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      ))}
    </Box>
  );
}
