import React from "react";
import { Switch, Route } from "react-router-dom";

import RoundList from "./RoundList";
import RoundDetails from "./RoundDetails";
import DraftRoundHome from "./draft";

type RoundHomeProps = {
  match: {
    url: string;
  };
};

export default function RoundHome(props: RoundHomeProps) {
  const { match } = props;
  return (
    <Switch>
      <Route exact path={`${match.url}`} component={RoundList} />
      <Route path={`${match.url}/draft`} component={DraftRoundHome} />
      <Route exact path={`${match.url}/:roundId`} component={RoundDetails} />
    </Switch>
  );
}
