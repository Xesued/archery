import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

import { Archer } from "../../model/archer";
import { Round, getArcherTargetScores } from "../../model/round";
import BodyContainer from "../../components/BodyContainer";
import Slideup from "../../components/transitions/Slideup";
import ModalHeader from "../../components/ModalHeader";

type RoundStatsProps = {
  round: Round;
  archer: Archer | null;
  isOpen: boolean;
  onClose: () => void;
};

const useStyles = makeStyles(theme => ({
  card: {
    marginTop: theme.spacing(2)
  },
  cardHeader: {
    fontSize: "1.1em",
    fontWeight: "bold"
  }
}));

export default function RoundStats(props: RoundStatsProps) {
  const { isOpen, onClose, archer, round } = props;
  const classes = useStyles();

  if (!archer) {
    return null;
  }

  const scores = getArcherTargetScores(round, archer);
  const archerMisses = scores.reduce(
    (acc, s) => acc + (s.score === 0 ? 1 : 0),
    0
  );
  const total = scores.reduce((acc, s) => acc + s.score, 0);
  return (
    <Dialog
      fullScreen
      open={isOpen}
      onClose={onClose}
      TransitionComponent={Slideup}
    >
      <ModalHeader title="Archer Stats" onClose={onClose} />
      <BodyContainer>
        <Typography variant="h5">{archer.name}'s Stats</Typography>
        <Card className={classes.card}>
          <CardContent>
            <Typography className={classes.cardHeader}>Overview</Typography>
            <Table size="small">
              <TableBody>
                <TableRow>
                  <TableCell>Score</TableCell>
                  <TableCell>{total}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Misses</TableCell>
                  <TableCell>{archerMisses}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </CardContent>
        </Card>
        <Card className={classes.card}>
          <CardContent>
            <Typography className={classes.cardHeader}>Targets</Typography>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell>Target</TableCell>
                  <TableCell>Yrd.</TableCell>
                  <TableCell>Score</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {scores.map((s, i) => (
                  <TableRow key={i}>
                    <TableCell>{s.name}</TableCell>
                    <TableCell>{s.yardage}</TableCell>
                    <TableCell>{s.score}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </BodyContainer>
    </Dialog>
  );
}
