import React from "react";
import { Switch, Route } from "react-router-dom";

import NewDraftRound from "./NewDraftRound";
import RoundFinish from "./RoundFinish";
import SelectArchers from "./SelectArchers";
import TargetHome from "./target";
import CurrentStandings from "./CurrentStandings";

type DraftRoundHomeProps = {
  match: {
    url: string;
  };
};

export default function DraftRoundHome(props: DraftRoundHomeProps) {
  const { match } = props;
  return (
    <Switch>
      <Route path={match.url} exact component={NewDraftRound} />
      <Route
        exact
        path={`${match.url}/select-archers`}
        component={SelectArchers}
      />
      <Route path={`${match.url}/summary`} exact component={CurrentStandings} />
      <Route path={`${match.url}/target`} component={TargetHome} />
      <Route path={`${match.url}/finish`} exact component={RoundFinish} />
    </Switch>
  );
}
