import React, { useState } from "react";
import { Redirect } from "react-router";
import uniqid from "uniqid";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";
import Fab from "@material-ui/core/Fab";
import CircularProgress from "@material-ui/core/CircularProgress";

import * as Round from "../../../model/round";
import { Archer } from "../../../model/archer";
import ArcherListItem from "../../../components/ArcherListItem";
import useRound from "../../../hooks/useDraftRound";
import auth from "../../../service/auth";
import FooterBar from "../../../components/FooterBar";
import SimpleContainer from "../../../components/SimpleContainer";

import SelectAvatarModal from "./SelectAvatarModal";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    addArcherContainer: {
      display: "flex"
    },
    addButton: {
      marginTop: 24,
      marginLeft: 42
    },
    rightIcon: {
      marginLeft: theme.spacing(1)
    },
    addInput: {
      width: "75%"
    },
    firstTarget: {
      marginTop: 64
    }
  })
);

export default function SelectArchers() {
  const [newArcherName, updateNewArcherName] = useState("");
  const [routeTo, updateRouteTo] = useState();
  const [round, updateRound] = useRound();
  const classes = useStyles();
  const [
    selectingAvatarArcher,
    setSelectingAvatarArcher
  ] = useState<Archer | null>(null);

  function addNewArcher(rnd: Round.Round) {
    let newRound = Round.addArcher(rnd, {
      id: uniqid.time(),
      name: newArcherName,
      imageIndex: Math.ceil(Math.random() * 100)
    });
    updateNewArcherName("");
    updateRound(newRound);
  }

  function removeArcher(archerIndex: number) {
    if (round) {
      let newRound = Round.removeArcherByIndex(round, archerIndex);
      updateRound(newRound);
    }
  }

  function updateRoundArcher(archer: Archer) {
    if (round) {
      let newRound = Round.updateArcher(round, archer);
      updateRound(newRound);
    }
  }

  function goToFirstTarget() {
    if (!round) {
      // TODO: Error here?
      return;
    }

    // Add the last archer if a name is entered.
    if (newArcherName) {
      const newRound = Round.addArcher(round, {
        id: uniqid.time(),
        name: newArcherName,
        imageIndex: Math.ceil(Math.random() * 100)
      });
      updateRound(newRound);
    }

    // If the round already has targets, just navigate to the
    // first target, otherwise we need to create one (ad-hoc).
    const targetId = round.targets.length ? round.targets[0].id : "new";
    updateRouteTo({ pathname: `/round/draft/target/${targetId}` });
  }

  function handleSelectedAvatar(index: number) {
    if (round && selectingAvatarArcher) {
      const newArcher = {
        ...selectingAvatarArcher,
        imageIndex: index
      };

      updateRoundArcher(newArcher);
    }
    setSelectingAvatarArcher(null);
  }

  if (!round) {
    return <CircularProgress />;
  }

  if (routeTo) {
    return <Redirect push to={routeTo.pathname} />;
  }

  const user = auth.getUserOrError();
  return (
    <Box>
      <SimpleContainer>
        <List>
          {round.archers.map((archer, index) => (
            <ArcherListItem
              key={archer.id}
              user={user}
              archer={archer}
              onAvatarClick={() => setSelectingAvatarArcher(archer)}
              onDelete={() => removeArcher(index)}
            />
          ))}
        </List>

        <div className={classes.addArcherContainer}>
          <TextField
            className={classes.addInput}
            id="archer-name"
            label="Name"
            value={newArcherName}
            onChange={event => updateNewArcherName(event.target.value)}
            margin="normal"
            variant="outlined"
          />
          <Fab
            size="small"
            className={classes.addButton}
            onClick={() => addNewArcher(round)}
            color="primary"
          >
            <AddIcon />
          </Fab>
        </div>

        <FooterBar>
          <Button
            onClick={goToFirstTarget}
            variant="contained"
            color="secondary"
            size="large"
          >
            Ready To Shoot
          </Button>
        </FooterBar>
      </SimpleContainer>
      <SelectAvatarModal
        isOpen={!!selectingAvatarArcher}
        onClose={() => setSelectingAvatarArcher(null)}
        onSelect={handleSelectedAvatar}
        selectedIndex={1}
      />
    </Box>
  );
}
