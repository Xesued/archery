import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import CircularProgress from "@material-ui/core/CircularProgress";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MoreIcon from "@material-ui/icons/MoreVert";
import ThumbsUp from "@material-ui/icons/ThumbUp";

import * as DraftDB from "../../../service/draft";
import * as RoundDB from "../../../service/db/rounds";
import useDraftRound from "../../../hooks/useDraftRound";
import * as Round from "../../../model/round";
import { Archer } from "../../../model/archer";
import FooterBar from "../../../components/FooterBar";
import RoundStatsModal from "../RoundStatsDialog";

const useStyles = makeStyles(theme => ({
  score: {
    marginRight: 5,
    marginBottom: 5,
    width: "15%",
    height: 30,
    paddingTop: 6,
    textAlign: "center"
  },
  scores: {
    display: "flex"
  },
  title: {
    fontSize: "1.5rem",
    textAlign: "center"
  },
  winner: {
    fontSize: "2rem",
    textAlign: "center",
    marginBottom: theme.spacing(8)
  },
  thumbContainer: {},
  thumb: {
    marginTop: theme.spacing(8),
    fontSize: "4rem"
  }
}));

/**
 * Show the results of the round.  It also saves it to the
 * database.
 *
 */
export default function RoundFinish() {
  const [isSavingRound, updateIsSavingRound] = useState(false);
  const [routeTo, setRouteTo] = useState<string | null>(null);
  const [statsArcher, setStatsArcher] = useState<Archer | null>(null);
  const [round] = useDraftRound();
  const classes = useStyles();

  async function finishAndSave() {
    if (round) {
      updateIsSavingRound(true);
      const finishedRound: Round.Round = {
        ...round,
        finished: Date.now()
      };
      const savedRound = await RoundDB.saveRound(finishedRound);
      DraftDB.saveDraftRound(null);
      setRouteTo(`/round/${savedRound.id}`);
    }
  }

  if (routeTo) {
    return <Redirect push to={routeTo} />;
  }

  if (!round) {
    return <CircularProgress />;
  }

  const archersById = round.archers.reduce<{ [archerId: string]: Archer }>(
    (acc, archer) => {
      acc[archer.id] = archer;
      return acc;
    },
    {}
  );
  const placements = Round.getPlacement(round);

  return (
    <Box>
      <Box display="flex" justifyContent="center">
        <ThumbsUp color="primary" className={classes.thumb} />
      </Box>
      <Typography variant="h5" className={classes.title}>
        Winner
      </Typography>
      <Typography variant="h5" className={classes.winner}>
        {archersById[placements[0][0]].name}
      </Typography>
      <Container>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>Archer</TableCell>
              <TableCell>Score</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {placements.map(archerScore => (
              <TableRow key={archerScore[0]}>
                <TableCell>{archersById[archerScore[0]].name}</TableCell>
                <TableCell>{archerScore[1]}</TableCell>
                <TableCell align="right">
                  <IconButton
                    onClick={() => setStatsArcher(archersById[archerScore[0]])}
                  >
                    <MoreIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Container>
      <FooterBar>
        {isSavingRound && <CircularProgress />}
        {!isSavingRound && (
          <Button
            variant="contained"
            color="secondary"
            onClick={() => finishAndSave()}
          >
            Finish
          </Button>
        )}
      </FooterBar>
      <RoundStatsModal
        isOpen={!!statsArcher}
        round={round}
        archer={statsArcher}
        onClose={() => setStatsArcher(null)}
      />
    </Box>
  );
}
