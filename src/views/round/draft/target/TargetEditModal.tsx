import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import * as Target from "../../../../model/target";
import * as Round from "../../../../model/round";
import * as Score from "../../../../model/score";
import * as Archer from "../../../../model/archer";
import BodyContainer from "../../../../components/BodyContainer";
import Slideup from "../../../../components/transitions/Slideup";
import ModalHeader from "../../../../components/ModalHeader";
import FooterBar from "../../../../components/FooterBar";

type TargetEditModalProps = {
  round: Round.Round;
  target: Target.Target;
  onCancel: () => void;
  onUpdate: (
    updatedTarget: Target.Target,
    updatedTargetScores: Score.TargetScores
  ) => void;
};

const useStyles = makeStyles(theme => ({
  card: {
    marginTop: theme.spacing(2)
  },
  fullWidthInput: {
    width: "100%"
  },
  cardHeader: {
    fontSize: "1.1em",
    fontWeight: "bold"
  },
  scoreButton: {}
}));

export default function TargetEditModal(props: TargetEditModalProps) {
  const { onCancel, onUpdate, target, round } = props;
  const classes = useStyles();

  const [updatedTarget, setUpdatedTarget] = useState<Target.Target>(
    Target.copy(target)
  );

  const [updatedTargetScores, setUpdatedTargetScores] = useState<
    Score.TargetScores
  >(Score.copyTargetScore(round.targetScores));

  function updateName(name: string) {
    const newTarget = Target.update(updatedTarget, { name });
    setUpdatedTarget(newTarget);
  }

  function updateYardage(yrd: string) {
    let yrdNum: number | undefined = parseInt(yrd, 10);
    if (Number.isNaN(yrdNum)) {
      yrdNum = undefined;
    }

    const newTarget = Target.update(updatedTarget, { yardage: yrdNum });
    setUpdatedTarget(newTarget);
  }

  function updateScore(archer: Archer.Archer, value: number) {
    const newScores = Score.updateArchersScore(
      updatedTargetScores,
      target.id,
      archer.id,
      value
    );

    setUpdatedTargetScores(newScores);
  }

  return (
    <Dialog fullScreen open onClose={onCancel} TransitionComponent={Slideup}>
      <ModalHeader title="Edit Target" onClose={onCancel} />
      <BodyContainer>
        <FormControl variant="filled" className={classes.fullWidthInput}>
          <TextField
            id="target-name"
            label="Target Name"
            value={updatedTarget.name}
            onChange={event => updateName(event.target.value)}
            margin="normal"
            variant="outlined"
          />
        </FormControl>
        <FormControl variant="filled" className={classes.fullWidthInput}>
          <TextField
            id="target-yardage"
            label="Yardage"
            value={updatedTarget.yardage || ""}
            onChange={event => updateYardage(event.target.value)}
            margin="normal"
            variant="outlined"
            type="number"
          />
        </FormControl>
        <Typography variant="h6">Scores</Typography>
        {round.targetScores[target.id].order.map(archerId => {
          const { score } = updatedTargetScores[target.id].scores[archerId];
          const archer = round.archers.find(a => a.id === archerId);
          if (!archer) return null;

          return (
            <React.Fragment key={archerId}>
              <Typography className={classes.cardHeader}>
                {archer.name}
              </Typography>
              {round.scoringMethod.values.map(sv => {
                return (
                  <Button
                    key={sv}
                    className={classes.scoreButton}
                    variant={"contained"}
                    color={sv === score ? "secondary" : "inherit"}
                    onClick={() => updateScore(archer, sv)}
                  >
                    {sv}
                  </Button>
                );
              })}
            </React.Fragment>
          );
        })}
        <FooterBar>
          <Button
            onClick={onCancel}
            variant="contained"
            style={{ marginRight: 15 }}
          >
            Cancel Edit
          </Button>
          <Button
            onClick={() => onUpdate(updatedTarget, updatedTargetScores)}
            variant="contained"
            color="secondary"
          >
            Save
          </Button>
        </FooterBar>
      </BodyContainer>
    </Dialog>
  );
}
