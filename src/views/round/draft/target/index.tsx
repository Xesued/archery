import React from "react";
import { Switch, Route } from "react-router-dom";

// import Target fro./old-Targetget";
import NewTarget from "./NewTarget";
import ScoreTargetArcher from "./ScoreTargetArcher";

type TargetHomeProps = {
  match: {
    url: string;
  };
};

export default function TargetHome(props: TargetHomeProps) {
  const { match } = props;
  return (
    <Switch>
      <Route path={`${match.url}/new`} exact component={NewTarget} />
      <Route
        path={`${match.url}/:targetId/archer/:archerId`}
        exact
        component={ScoreTargetArcher}
      />
    </Switch>
  );
}
