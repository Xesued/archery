import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import ArcherListItem from "../../../../components/ArcherListItem";

import useRound from "../../../../hooks/useDraftRound";
import { CircularProgress, Button } from "@material-ui/core";
import * as Round from "../../../../model/round";
import { getNextArcherId } from "../../../../model/archer";
import FooterBar from "../../../../components/FooterBar";
import SimpleContainer from "../../../../components/SimpleContainer";

type NewTargetProps = {
  match: any;
  history: any;
};

const useStyles = makeStyles({
  fullWidthInput: {
    width: "100%"
  },
  turnOrder: {
    marginTop: 24
  }
});

export default function Target(props: NewTargetProps) {
  const { match, history } = props;
  const { targetId } = match.params;
  const [round, updateRound] = useRound();
  const classes = useStyles();
  const [moveToNext, updateMoveToNext] = useState(false);

  if (!round) {
    return <CircularProgress />;
  }

  const target = round.targets.find(t => t.id === targetId);
  if (!target) {
    return <div>Couldn't find target in round</div>;
  }

  if (!round.targetScores[targetId]) {
    updateRound(Round.initTargetScores(round, targetId));
    return <CircularProgress />;
  }

  const archerOrder = round.targetScores[targetId].order;

  async function scoreTarget() {
    updateMoveToNext(true);
  }

  if (moveToNext) {
    const nextArcher = getNextArcherId(round, targetId);
    if (nextArcher) {
      let newTargetRoute = `/round/draft/target/${targetId}/archer/${nextArcher}`;
      history.push({
        pathname: newTargetRoute
      });
    } else {
      throw Error("Couldn't get archer from target");
    }
  }

  return (
    <Box>
      <SimpleContainer>
        Name: {target.name}
        <br />
        Yrd: {target.yardage}
        {target.image && <img src={target.image} style={{ width: "100%" }} />}
        <Typography variant="h5" className={classes.turnOrder}>
          Turn Order
        </Typography>
        {archerOrder.map(archerId => {
          const archer = round.archers.find(a => a.id === archerId);
          return archer && <ArcherListItem key={archerId} archer={archer} />;
        })}
      </SimpleContainer>
      <FooterBar>
        <Button
          variant="contained"
          color="secondary"
          size="large"
          onClick={() => scoreTarget()}
        >
          Score Target
        </Button>
      </FooterBar>
    </Box>
  );
}
