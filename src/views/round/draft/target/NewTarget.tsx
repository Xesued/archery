import React, { useState } from "react";
import uniqid from "uniqid";

import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import useRound from "../../../../hooks/useDraftRound";
import * as Round from "../../../../model/round";
import * as Archer from "../../../../model/archer";
import FooterBar from "../../../../components/FooterBar";
import ArcherListItem from "../../../../components/ArcherListItem";
import SimpleContainer from "../../../../components/SimpleContainer";

type NewTargetProps = {
  match: any;
  history: any;
};

const useStyles = makeStyles({
  fullWidthInput: {
    width: "100%"
  },
  turnOrder: {
    marginTop: 24
  }
});

export default function NewTarget(props: NewTargetProps) {
  const { history } = props;
  const [round, updateRound] = useRound();
  const classes = useStyles();
  const [yardage, updateYardage] = useState("");
  const [targetName, updateTargetName] = useState("");

  async function saveTarget() {
    if (round) {
      const targetIndex = round.targets.length;
      const targetId = uniqid.time();
      const archerOrder = Round.getArcherOrder(round, round.targets.length);
      const newRound = Round.addTarget(
        round,
        {
          id: targetId,
          name: targetName || `Target #${targetIndex}`,
          yardage: parseInt(yardage, 10) || -1,
          image: null
        },
        archerOrder.map(ao => ao.id)
      );

      await updateRound(newRound);

      const nextArcher = Archer.getNextArcherId(newRound, targetId);
      if (nextArcher) {
        let newTargetRoute = `/round/draft/target/${targetId}/archer/${nextArcher}`;
        history.push({
          pathname: newTargetRoute
        });
      } else {
        console.error("Derp, no first archer?");
      }
    }
  }

  if (!round) {
    return <CircularProgress />;
  }

  const archerOrder = Round.getArcherOrder(round, round.targets.length);
  return (
    <Box>
      <SimpleContainer>
        <FormControl variant="filled" className={classes.fullWidthInput}>
          <TextField
            id="target-name"
            label="Target Name"
            value={targetName}
            onChange={event => updateTargetName(event.target.value)}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              shrink: true
            }}
          />
        </FormControl>
        <FormControl variant="filled" className={classes.fullWidthInput}>
          <TextField
            id="target-yardage"
            label="Yardage"
            value={yardage}
            onChange={event => updateYardage(event.target.value)}
            margin="normal"
            variant="outlined"
            type="number"
            InputLabelProps={{
              shrink: true
            }}
          />
        </FormControl>
        <Typography variant="h5" className={classes.turnOrder}>
          Turn Order
        </Typography>
        {archerOrder.map(archerOrder => {
          const archer = round.archers.find(a => a.id === archerOrder.id);
          return archer && <ArcherListItem key={archer.id} archer={archer} />;
        })}
      </SimpleContainer>
      <FooterBar>
        <Button
          variant="contained"
          color="secondary"
          size="large"
          onClick={() => saveTarget()}
        >
          Next
        </Button>
      </FooterBar>
    </Box>
  );
}
