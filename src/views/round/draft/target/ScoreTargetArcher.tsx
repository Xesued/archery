import React from "react";

import { Theme, makeStyles, createStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";

import ScoreButton from "../../../../components/ScoreButton";
import ArcherAvatar from "../../../../components/ArcherAvatar";

import useDraftRound from "../../../../hooks/useDraftRound";
import * as Round from "../../../../model/round";
import { getNextArcherId } from "../../../../model/archer";
import SimpleContainer from "../../../../components/SimpleContainer";
import ScreenTitle from "../../../../components/ScreenTitle";

type ScoreTargetProps = {
  match: any;
  history: any;
};

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    archerInfo: {
      margin: 25
    },
    avatar: {
      width: 124,
      height: 124
    }
  })
);

export default function ScoreTargetArcher(props: ScoreTargetProps) {
  const { match, history } = props;
  const { targetId, archerId } = match.params;
  const [round, updateRound] = useDraftRound();
  const classes = useStyle();

  if (!round) {
    return <CircularProgress />;
  }

  const currentArcher = round.archers.find(a => a.id === archerId);
  if (!currentArcher) {
    return <div>Archer no exist</div>;
  }

  const target = round.targets.find(t => t.id === targetId);
  if (!target) {
    return <div>Target don't exist man</div>;
  }

  const nextArcher = getNextArcherId(round, targetId, archerId);
  async function scoreArcher(value: number) {
    if (round) {
      const onTarget = round.targets.find(t => t.id === targetId);
      if (onTarget && currentArcher) {
        const newRound = Round.addScore(round, onTarget, currentArcher.id, {
          score: value,
          merits: []
        });

        await updateRound(newRound);

        // No more archers...
        let pathname;
        if (nextArcher) {
          pathname = `/round/draft/target/${targetId}/archer/${nextArcher}`;
        } else {
          pathname = `/round/draft/summary`;
        }

        history.push({ pathname });
      }
    }
  }

  return (
    <Box>
      <ScreenTitle title={target.name} />
      <SimpleContainer>
        <Box
          className={classes.archerInfo}
          display="flex"
          flexDirection="column"
        >
          <Box alignSelf="center">
            <ArcherAvatar archer={currentArcher} avatarClass={classes.avatar} />
          </Box>
          <Box alignSelf="center">
            <Typography variant="h5">{currentArcher.name}</Typography>
          </Box>
        </Box>
        <Grid container justify="center" spacing={4}>
          {round.scoringMethod.values.map(value => (
            <Grid item key={value}>
              <ScoreButton
                key={value}
                value={value}
                onClick={() => scoreArcher(value)}
              />
            </Grid>
          ))}
        </Grid>
      </SimpleContainer>
    </Box>
  );
}
