import React, { useState } from "react";
import { Link as RouterLink, Redirect } from "react-router-dom";

import { Theme, makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CircularProgress from "@material-ui/core/CircularProgress";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import MoreVertIcon from "@material-ui/icons/MoreVert";

import * as DraftDb from "../../../service/draft";
import * as Round from "../../../model/round";
import * as Target from "../../../model/target";
import * as Archer from "../../../model/archer";
import * as Score from "../../../model/score";
import useDraftRound from "../../../hooks/useDraftRound";
import ArcherAvatar from "../../../components/ArcherAvatar";
import RoundStatsModal from "../RoundStatsDialog";
import TargetEditModal from "./target/TargetEditModal";
import FooterBar from "../../../components/FooterBar";
import SimpleContainer from "../../../components/SimpleContainer";
import AlertDialog from "../../../components/AlertDialog";

type CurrentStandingsProps = {
  match: any;
};

const useStyles = makeStyles((theme: Theme) => ({
  score: {
    marginRight: 5,
    marginBottom: 5,
    width: "15%",
    height: 30,
    paddingTop: 6,
    textAlign: "center"
  },
  scores: {
    display: "flex"
  },
  archerCard: {
    marginTop: 8
  },
  archerName: {
    marginTop: 6,
    marginLeft: 12
  },
  archerContent: {
    marginLeft: 52
  },
  archerScore: {
    fontSize: "1.4em",
    fontWeight: "bold"
  },
  avatar: {
    fontSize: "1.0em",
    fontWeight: "bold",
    color: theme.palette.text.secondary,
    backgroundColor: theme.palette.secondary.main
  },
  card: {
    marginBottom: theme.spacing(3)
  },
  header: {
    marginTop: theme.spacing(3)
  }
}));

export default function CurrentStandings(props: CurrentStandingsProps) {
  const [round] = useDraftRound();
  const [routeTo, updateRouteTo] = useState();
  const [isEditDialogOpen, setIsEditDialogOpen] = useState(false);
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const [selectedTarget, setSelectedTarget] = useState<Target.Target | null>(
    null
  );
  const [statsArcher, setStatsArcher] = useState<Archer.Archer | null>(null);
  const [menuAnchorEl, setMenuAnchorEl] = useState<HTMLElement | null>(null);
  const classes = useStyles();

  if (!round) {
    return <CircularProgress />;
  }

  function moveToNextTarget(rnd: Round.Round) {
    updateRouteTo({ pathname: `/round/draft/target/new` });
  }

  if (routeTo) {
    return <Redirect push to={routeTo.pathname} />;
  }

  const archersScores = round.archers.map(a => ({
    archer: a,
    score: Round.getRoundScoreForArcher(round, a.id)
  }));

  archersScores.sort((a, b) => {
    if (a.score < b.score) return 1;
    if (a.score > b.score) return -1;
    return 0;
  });

  function handleClose() {
    setMenuAnchorEl(null);
  }

  function handleMenuClick(
    event: React.MouseEvent<HTMLElement>,
    target: Target.Target
  ) {
    setSelectedTarget(target);
    setMenuAnchorEl(event.currentTarget);
  }

  function handleEditMenuItem() {
    setIsEditDialogOpen(true);
    setMenuAnchorEl(null);
  }

  function cancelTargetEdit() {
    setIsEditDialogOpen(false);
  }

  function handleDeleteMenuItem() {
    setIsDeleteDialogOpen(true);
    setMenuAnchorEl(null);
  }

  function cancelTargetDelete() {
    setIsDeleteDialogOpen(false);
  }

  function updateTargetAndScores(
    updatedTarget: Target.Target,
    targetScores: Score.TargetScores
  ) {
    if (round) {
      let newRound = Round.updateTarget(round, updatedTarget);
      newRound = Round.updateTargetScores(newRound, targetScores);
      DraftDb.saveDraftRound(newRound);
      setIsEditDialogOpen(false);
    }
    setSelectedTarget(null);
  }

  function confirmDeleteTarget() {
    if (round && selectedTarget) {
      let newRound = Round.removeTarget(round, selectedTarget.id);
      DraftDb.saveDraftRound(newRound);
      setIsDeleteDialogOpen(false);
    }
    setSelectedTarget(null);
  }

  const targets = [...round.targets].reverse();
  return (
    <Box>
      <SimpleContainer>
        <Typography variant="h6" gutterBottom>
          Overview
        </Typography>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Score</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {archersScores.map(archerScore => (
              <TableRow
                key={archerScore.archer.id}
                onClick={() => setStatsArcher(archerScore.archer)}
              >
                <TableCell>
                  <ArcherAvatar archer={archerScore.archer} />
                </TableCell>
                <TableCell>{archerScore.archer.name}</TableCell>
                <TableCell>{archerScore.score}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <Typography className={classes.header} variant="h6" gutterBottom>
          Targets
        </Typography>
        {targets.map((target, index) => {
          const scores = round.targetScores[target.id];
          return (
            <Card className={classes.card} key={target.id}>
              <CardHeader
                title={target.name}
                subheader={`${target.yardage} yrds.`}
                avatar={
                  <Avatar aria-label="recipe" className={classes.avatar}>
                    #{targets.length - index}
                  </Avatar>
                }
                action={
                  <IconButton
                    aria-label="settings"
                    onClick={e => handleMenuClick(e, target)}
                  >
                    <MoreVertIcon />
                  </IconButton>
                }
              />
              <CardContent>
                <Table size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell>Name</TableCell>
                      <TableCell>Score</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {archersScores.map(archerScore => (
                      <TableRow key={archerScore.archer.id}>
                        <TableCell>{archerScore.archer.name}</TableCell>
                        <TableCell>
                          {scores.scores[archerScore.archer.id].score}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </CardContent>
            </Card>
          );
        })}
      </SimpleContainer>
      <FooterBar>
        <Button
          component={RouterLink}
          to={`/round/draft/finish`}
          variant="contained"
          style={{ marginRight: 15 }}
        >
          Done Shooting
        </Button>
        <Button
          onClick={() => moveToNextTarget(round)}
          variant="contained"
          color="secondary"
        >
          Next Target
        </Button>
      </FooterBar>
      {isEditDialogOpen && selectedTarget && (
        <TargetEditModal
          round={round}
          target={selectedTarget}
          onCancel={cancelTargetEdit}
          onUpdate={updateTargetAndScores}
        />
      )}
      <AlertDialog
        isOpen={isDeleteDialogOpen}
        title="Really Delete Target?"
        text="You can't undo this action, but you add in a new one later"
        onCancel={cancelTargetDelete}
        onConfirm={confirmDeleteTarget}
      />

      <RoundStatsModal
        isOpen={!!statsArcher}
        round={round}
        archer={statsArcher}
        onClose={() => setStatsArcher(null)}
      />
      <Menu
        id="simple-menu"
        anchorEl={menuAnchorEl}
        keepMounted
        open={Boolean(menuAnchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleEditMenuItem}>Edit</MenuItem>
        <Divider />
        <MenuItem onClick={handleDeleteMenuItem}>Delete</MenuItem>
      </Menu>
    </Box>
  );
}
