import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import LandscapeIcon from "@material-ui/icons/Landscape";

import * as Score from "../../../model/score";
import * as Round from "../../../model/round";
import auth from "../../../service/auth";
import * as DraftDB from "../../../service/draft";
import getRandomName from "../../../service/nameGenerator";

import SimpleContainer from "../../../components/SimpleContainer";
import FooterBar from "../../../components/FooterBar";

import useGeoLocation from "../../../hooks/useGeoLocation";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    field: {
      width: "100%",
      marginBottom: "24px"
    },
    rightIcon: {
      marginLeft: theme.spacing(1)
    },
    setupArchers: {
      marginTop: 64
    },
    container: {
      paddingTop: theme.spacing(2)
    },
    iconContainer: {
      margin: "auto",
      width: 125
    },
    icon: {
      width: 125,
      height: 125
    }
  })
);

/**
 * Creates a new Round in the DraftDB.  This is where
 * we setup the name and type of round.
 *
 */
export default function NewDraftRound() {
  const user = auth.getUserOrError();
  const classes = useStyles();
  const location = useGeoLocation();
  const today = new Date();
  const [roundName, updateRoundName] = useState(
    `${getRandomName()} ${today.getMonth() +
      1}/${today.getDate()}/${today.getFullYear()}`
  );
  const [
    newlyCreatedRound,
    updateNewlyCreatedRound
  ] = useState<Round.Round | null>(null);
  const [scoringType, updateScoringType] = useState("IBO");

  function createNewRound() {
    const round = Round.createRound(
      roundName,
      user.uid,
      Score.getScoringMethodFromType(scoringType),
      [],
      [
        {
          id: user.uid,
          name: user.displayName || "",
          imageIndex: Math.ceil(Math.random() * 100) - 1
        }
      ],
      location
    );

    DraftDB.saveDraftRound(round);
    updateNewlyCreatedRound(round);
  }

  if (newlyCreatedRound) {
    return (
      <Redirect
        push
        to={{
          pathname: `/round/draft/select-archers`
        }}
      />
    );
  }

  return (
    <Box>
      <SimpleContainer>
        <Box className={classes.iconContainer}>
          <LandscapeIcon className={classes.icon} />
        </Box>
        <TextField
          id="round-name"
          variant="outlined"
          className={classes.field}
          label="Round Name"
          value={roundName}
          onChange={event => updateRoundName(event.target.value)}
          margin="normal"
        />
        <React.Fragment>
          <FormLabel component="span">Scoring Type</FormLabel>
          <RadioGroup
            className={classes.field}
            aria-label="Scoring Type"
            name="scoring-type"
            value={scoringType}
            onChange={event =>
              updateScoringType((event.target as HTMLInputElement).value)
            }
          >
            <FormControlLabel value="IBO" control={<Radio />} label="IBO" />
            <FormControlLabel value="ASA" control={<Radio />} label="ASA" />
          </RadioGroup>
        </React.Fragment>
      </SimpleContainer>
      <FooterBar>
        <Button
          variant="contained"
          color="secondary"
          size="large"
          onClick={() => createNewRound()}
        >
          Setup Archers
        </Button>
      </FooterBar>
    </Box>
  );
}
