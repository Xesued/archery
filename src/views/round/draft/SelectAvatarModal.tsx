import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Dialog from "@material-ui/core/Dialog";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { TransitionProps } from "@material-ui/core/transitions";
import Avatar from "@material-ui/core/Avatar";

import images from "../../../images/images";
import BodyContainer from "../../../components/BodyContainer";

type SelectAvatarProps = {
  isOpen: boolean;
  selectedIndex: number;
  onClose: () => void;
  onSelect: (avatarIndex: number) => void;
};

const Transition = React.forwardRef<unknown, TransitionProps>(
  function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  }
);

const useStyles = makeStyles(theme => ({
  appbar: {
    position: "relative",
    marginBottom: theme.spacing(2)
  },
  avatarGridItem: {
    // width: 124
  },
  avatar: {
    width: 64,
    height: 64
  }
}));

export default function SelectAvatar(props: SelectAvatarProps) {
  const { isOpen, onClose, onSelect } = props;
  const classes = useStyles();

  return (
    <Dialog
      fullScreen
      open={isOpen}
      onClose={onClose}
      TransitionComponent={Transition}
    >
      <AppBar className={classes.appbar}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={onClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h6">Select Avatar</Typography>
        </Toolbar>
      </AppBar>
      <BodyContainer>
        <Grid container spacing={2} justify="center">
          {images.map((image, imageIndex) => (
            <Grid
              item
              key={imageIndex}
              className={classes.avatarGridItem}
              justify="center"
            >
              <Avatar
                className={classes.avatar}
                src={image}
                onClick={() => onSelect(imageIndex)}
              />
            </Grid>
          ))}
        </Grid>
      </BodyContainer>
    </Dialog>
  );
}
