import React, { useEffect } from "react";
import { Theme, makeStyles } from "@material-ui/core/styles";

import { showSignin } from "../service/auth";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    marginTop: theme.spacing(9)
  }
}));
type LoginProps = {};

export default function Login(props: LoginProps) {
  const classes = useStyles();

  useEffect(() => {
    showSignin("#login");
  }, []);

  return (
    <div className={classes.root}>
      <div id="login" />
    </div>
  );
}
