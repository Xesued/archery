import React from "react";
import { Link as RouterLink } from "react-router-dom";

import { Theme, makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import { VERSION } from "../version";

import { ReactComponent as VitalLogo } from "../vital-shot-outer-ring.svg";

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    marginTop: theme.spacing(8),
    height: "70vh"
  },
  newRoundLink: {
    marginTop: 64
  },
  logoContainer: {
    width: "75%"
  },
  logo: {}
}));

export default function Home() {
  const classes = useStyles();
  return (
    <Box
      display="flex"
      flexDirection="column"
      justifyContent="space-around"
      className={classes.container}
    >
      <Box alignSelf="center" className={classes.logoContainer}>
        <VitalLogo className={classes.logo} />
      </Box>
      <Box alignSelf="center">
        <Link
          component={RouterLink}
          to="/round/draft"
          className={classes.newRoundLink}
        >
          <Button variant="contained" color="secondary" size="large">
            Shoot A Round
          </Button>
        </Link>
      </Box>
      <Box alignSelf="center">v{VERSION}</Box>
    </Box>
  );
}
