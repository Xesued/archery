import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";

import ArcherAvatar from "./ArcherAvatar";
import { Archer } from "../model/archer";

type ArcherListItemProps = {
  user?: firebase.User;
  archer: Archer;
  onDelete?: () => void;
  onAvatarClick?: () => void;
};

const useStyles = makeStyles(theme => ({
  avatarButton: {
    padding: theme.spacing(0)
  }
}));

export default function ArcherListItem(props: ArcherListItemProps) {
  const { archer, onDelete, user, onAvatarClick } = props;
  const isArcherUser = user ? user.uid === archer.id : false;
  const classes = useStyles();

  return (
    <ListItem>
      <ListItemAvatar>
        <IconButton className={classes.avatarButton} onClick={onAvatarClick}>
          <ArcherAvatar archer={archer} />
        </IconButton>
      </ListItemAvatar>
      <ListItemText primary={archer.name} />
      {onDelete && !isArcherUser && (
        <ListItemSecondaryAction>
          <IconButton edge="end" aria-label="Delete" onClick={onDelete}>
            <DeleteIcon color="secondary" />
          </IconButton>
        </ListItemSecondaryAction>
      )}
    </ListItem>
  );
}
