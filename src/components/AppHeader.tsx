import React from "react";
import { useLocation } from "react-router-dom";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";

import MenuIcon from "@material-ui/icons/Menu";

type AppHeaderProps = {
  onOpenDrawer: () => void;
};

type StringMap = { [key: string]: string };
const routeToHeaderMap: StringMap = {
  "/round/draft/summary": "Current Standings",
  "/round/draft/finish": "Round Finished",
  "/round/draft/target/new": "Next Target",
  "/round/draft/target/.*/archer/.*": "Score Archer",
  "/round/draft/target/.*": "Turn Order",
  "/round/draft/select-archers": "Select Archers",
  "/round/draft": "New Round",
  "/round/.*": "Round Details",
  "/round": "Past Rounds",
  "/": "Vital Shot"
};

const getHeaderFromPath = (path: string): string => {
  const routes = Object.keys(routeToHeaderMap);
  const foundRoute = routes.find(routePath => new RegExp(routePath).test(path));

  if (foundRoute) {
    return routeToHeaderMap[foundRoute];
  }

  return "Vital Shot (tm)";
};

export default function AppHeader(props: AppHeaderProps) {
  const { onOpenDrawer } = props;
  const location = useLocation();

  const header = getHeaderFromPath(location.pathname);
  return (
    <AppBar position="fixed">
      <Toolbar>
        <IconButton
          onClick={onOpenDrawer}
          edge="start"
          color="inherit"
          aria-label="menu"
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" color="inherit" noWrap>
          {header}
        </Typography>
      </Toolbar>
    </AppBar>
  );
}
