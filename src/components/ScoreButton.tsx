import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

type ScoreButtonProps = {
  value: number;
  onClick: () => void;
};

const useStyles = makeStyles({
  scoreButton: {
    width: "25%",
    height: 75
  }
});

export default function ScoreButton(props: ScoreButtonProps) {
  const { value, ...rest } = props;
  const classes = useStyles();

  return (
    <Button
      className={classes.scoreButton}
      variant={"contained"}
      color="inherit"
      {...rest}
    >
      {value}
    </Button>
  );
}
