import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

type ScreenTitleProps = {
  title: string;
  subtitle?: string;
};

const useStyles = makeStyles(theme => ({
  title: {
    margin: theme.spacing(2)
  },
  subtitle: {
    margin: theme.spacing(2),
    marginTop: -theme.spacing(1)
  }
}));

export default function ScreenTitle(props: ScreenTitleProps) {
  const { title, subtitle } = props;
  const classes = useStyles();
  return (
    <Box>
      <Typography variant="h5" className={classes.title}>
        {title}
      </Typography>
      {subtitle && (
        <Typography variant="subtitle2" className={classes.subtitle}>
          {subtitle}
        </Typography>
      )}
    </Box>
  );
}
