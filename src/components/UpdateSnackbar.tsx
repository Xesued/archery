import React, { useState, useEffect } from "react";

import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import RefreshIcon from "@material-ui/icons/Refresh";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    close: {
      padding: theme.spacing(0.5)
    }
  })
);

type UpdateSnackbarProps = {
  hasUpdate: boolean;
  onSkipWaiting: () => void;
};

export default function UpdateSnackbar(props: UpdateSnackbarProps) {
  const { hasUpdate, onSkipWaiting } = props;
  const classes = useStyles();
  const [open, setOpen] = useState(hasUpdate);
  console.log("render snack");
  useEffect(() => {
    setOpen(hasUpdate);
  }, [hasUpdate]);

  function handleClose(
    event: React.SyntheticEvent | React.MouseEvent,
    reason?: string
  ) {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  }

  function handleSkipWaiting() {
    onSkipWaiting();
  }

  return (
    <div>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left"
        }}
        open={open}
        onClose={handleClose}
        ContentProps={{
          "aria-describedby": "message-id"
        }}
        message={<span id="message-id">Update Available</span>}
        action={[
          <Button
            key="reload"
            aria-label="Reload"
            color="inherit"
            className={classes.close}
            onClick={handleSkipWaiting}
          >
            Reload
            <RefreshIcon />
          </Button>
        ]}
      />
    </div>
  );
}
