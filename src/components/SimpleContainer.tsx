import React from "react";
import Container from "@material-ui/core/Container";
import { makeStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    paddingTop: theme.spacing(9),
    paddingBottom: theme.spacing(6)
  }
}));

type SimpleContainerProps = {
  children: React.ReactNode;
};

export default function SimpleContainer(props: SimpleContainerProps) {
  const { children } = props;
  const classes = useStyles();

  return <Container className={classes.root}>{children}</Container>;
}
