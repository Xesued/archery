import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

type FooterBarProps = {
  children: React.ReactNode;
};

const useStyles = makeStyles(theme => ({
  root: {
    top: "auto",
    bottom: 0
  },
  toolbar: {
    textAlign: "center",
    justifyContent: "flex-end"
  }
}));

export default function FooterBar(props: FooterBarProps) {
  const { children } = props;
  const classes = useStyles();
  return (
    <AppBar position="fixed" color="default" className={classes.root}>
      <Toolbar className={classes.toolbar}>{children}</Toolbar>
    </AppBar>
  );
}
