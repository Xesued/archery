import React from "react";
import { Route, Redirect } from "react-router-dom";
import useAuth from "../hooks/useAuth";
import { CircularProgress } from "@material-ui/core";

export default function PrivateRoute({ component: Component, ...rest }: any) {
  const user = useAuth();

  return (
    <Route
      {...rest}
      render={props => {
        if (user instanceof Error) {
          return (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          );
        }

        if (user) {
          return <Component {...props} />;
        }

        return <CircularProgress />;
      }}
    />
  );
}
