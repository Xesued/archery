import React from "react";
import Avatar from "@material-ui/core/Avatar";

import { Archer } from "../model/archer";
import images from "../images/images";

type ArcherAvatarProps = {
  archer: Archer;
  avatarClass?: string;
};

export default function ArcherAvatar({
  archer,
  avatarClass = ""
}: ArcherAvatarProps) {
  return <Avatar className={avatarClass} src={images[archer.imageIndex % 5]} />;
}
