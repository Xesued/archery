import React from "react";
import Container from "@material-ui/core/Container";

type BodyContainerProps = {
  children: React.ReactNode;
};

export default function BodyContainer(props: BodyContainerProps) {
  return <Container>{props.children}</Container>;
}
