import React from "react";

import ImageIcon from "@material-ui/icons/Image";
import { Grid } from "@material-ui/core";

export default function NoImagePlaceholder() {
  return (
    <Grid container justify="center" direction="column">
      <Grid item>
        <Grid container justify="center">
          <Grid item>
            <ImageIcon fontSize="large" />
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid container justify="center">
          <Grid item>Add Image</Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
