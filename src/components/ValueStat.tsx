import React from "react";

import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";

type ValueStatProps = {
  value: string;
  label: string;
};

const useStyles = makeStyles(theme => ({
  root: {
    padding: "12px 8px",
    textAlign: "center"
  },
  value: {
    fontSize: "2rem"
  },
  label: {
    fontSize: "0.8rem"
  }
}));

export default function ValueStat(props: ValueStatProps) {
  const { value, label } = props;
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Box className={classes.value}>{value}</Box>
      <Box className={classes.label}>{label}</Box>
    </Paper>
  );
}
