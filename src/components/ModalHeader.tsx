import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

type ModalHeaderProps = {
  title: string;
  onClose: () => void;
};

export default function ModalHeader(props: ModalHeaderProps) {
  const { onClose, title } = props;
  const classes = useStyle();

  return (
    <AppBar className={classes.appbar}>
      <Toolbar>
        <IconButton
          edge="start"
          color="inherit"
          onClick={onClose}
          aria-label="close"
        >
          <CloseIcon />
        </IconButton>
        <Typography variant="h6">{title}</Typography>
      </Toolbar>
    </AppBar>
  );
}

const useStyle = makeStyles(theme => ({
  appbar: {
    position: "relative",
    marginBottom: theme.spacing(2)
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  }
}));
