import React, { ChangeEvent, useEffect, useState } from "react";
import * as firebase from "firebase/app";

import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";
import Paper from "@material-ui/core/Paper";

import NoImagePlaceholder from "./NoImagePlaceholder";

const useStyles = makeStyles(theme => ({
  imageContainer: {
    width: "100%",
    minHeight: 50,
    color: theme.palette.grey[500]
  },
  courseImage: {
    width: "100%"
  }
}));
interface HTMLInputEvent extends ChangeEvent {
  target: HTMLInputElement & EventTarget;
}

type FileUploadProps = {
  /**
   * The source of the current image src if set
   */
  currentSrc: string | null;

  /**
   * where sould we store this image
   */
  rootPath: string;

  /**
   * The name of the file stored.  If no name is given,
   * the choose file name is used.
   */
  fileName?: string;

  /**
   * Called when a file is selected.
   */
  onSelect?: () => void;

  /**
   * Called when the file is uploaded.  Returns the
   * url string to access it.
   */
  onUpload: (url: string) => void;
};

export default function FileUpload(props: FileUploadProps) {
  const { rootPath, fileName, onUpload, currentSrc } = props;
  const [storageRef, setStorageRef] = useState();
  const [isUploading, setIsUploading] = useState(false);
  const classes = useStyles();

  useEffect(() => {
    const ref = firebase.storage().ref();
    setStorageRef(ref);
  }, []);

  const handleFileChange = async (e: HTMLInputEvent) => {
    e.preventDefault();
    e.stopPropagation();

    if (storageRef && e.target && e.target.files) {
      const file = e.target.files[0];
      const metadata = {
        contentType: file.type
      };

      const storedFileName = fileName || file.name;
      const fullFileNamePath = `images/${rootPath}/${storedFileName}`;
      setIsUploading(true);

      try {
        const snapshot = await storageRef
          .child(fullFileNamePath)
          .put(file, metadata);

        // Let's get a download URL for the file.
        const fileUrl = await snapshot.ref.getDownloadURL();
        onUpload(fileUrl);
        setIsUploading(false);
      } catch (error) {
        // [START onfailure]
        console.error("Upload failed:", error);
        // [END onfailure]
      }
    }
  };

  return (
    <Paper className={classes.imageContainer}>
      <Box>
        {isUploading && <CircularProgress />}
        {!isUploading && currentSrc && (
          <img className={classes.courseImage} src={currentSrc} />
        )}
        {!isUploading && !currentSrc && <NoImagePlaceholder />}
      </Box>
      <Box>
        {!isUploading && <input type="file" onChange={handleFileChange} />}
      </Box>
    </Paper>
  );
}
