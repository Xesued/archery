import * as firebase from "firebase";
import * as firebaseui from "firebaseui";

const uiConfig = {
  signInSuccessUrl: "/",
  signInOptions: [
    // Leave the lines as is for the providers you want to offer your users.
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID
  ],

  // tosUrl and privacyPolicyUrl accept either url string or a callback
  // function.
  // Terms of service url/callback.
  tosUrl: "<your-tos-url>",

  // Privacy policy url/callback.
  privacyPolicyUrl: function() {
    window.location.assign("<your-privacy-policy-url>");
  }
};

let ui: firebaseui.auth.AuthUI;

export function showSignin(containerSelector: string): void {
  if (ui) {
    ui.start(containerSelector, uiConfig);
    return;
  }

  // Initialize the FirebaseUI Widget using Firebase.
  ui = new firebaseui.auth.AuthUI(firebase.auth());

  // The start method will wait until the DOM is loaded.
  ui.start(containerSelector, uiConfig);
}

class Auth {
  user: firebase.User | null = null;

  async isAuthenticated(): Promise<boolean> {
    if (this.user) {
      return true;
    }

    return new Promise(resolve => {
      firebase.auth().onAuthStateChanged(user => {
        this.user = user;
        resolve(!!this.user);
      });
    });
  }

  getUserOrError(): firebase.User {
    if (!this.user) {
      throw Error("Have not fetched user yet.");
    }

    return this.user;
  }
}

const auth = new Auth();
export default auth;
