import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";
import "firebase/auth";

const FIREBASE_CONFIG = {
  apiKey: "AIzaSyBrIDZEVl-8hnpPqjClVk1QwJZAtsfoIWQ",
  authDomain: "vital-shot.firebaseapp.com",
  databaseURL: "https://vital-shot.firebaseio.com",
  projectId: "vital-shot",
  storageBucket: "vital-shot.appspot.com",
  messagingSenderId: "1011586595508",
  appId: "1:1011586595508:web:8fc11e75a01bd2ab"
};

// Initialize Firebase

firebase.initializeApp(FIREBASE_CONFIG);

const db = firebase.firestore();
db.enablePersistence()
  .then(() => console.log("Persistance enabled"))
  .catch(err => {
    if (err.code === "failed-precondition") {
      console.error("Too many tabs open.  Can only enabled in one tab");
    } else if (err.code === "unimplemented") {
      console.error("Browser does not support offline mode");
    } else {
      console.error("Error setting up persistence", err.code);
    }
  });

export default db;
