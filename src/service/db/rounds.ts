import db from "../firebase";
import * as Round from "../../model/round";

/**
 * Saves a round to the database.
 *
 * Returns the newly created id of the document.
 */
export async function saveRound(round: Round.Round): Promise<Round.Round> {
  return round.id ? updateRound(round) : createRound(round);
}

async function updateRound(round: Round.Round): Promise<Round.Round> {
  if (!round.id) {
    throw Error("Round does not have an ID.  Cannot update.");
  }

  const { id, ...roundWithoutId } = round;
  await db
    .collection("rounds")
    .doc(id)
    .set(roundWithoutId);

  return round;
}

export async function deleteRound(round: Round.Round): Promise<void> {
  const { id } = round;
  if (!id) {
    throw Error("Round does not have an ID.  Cannot update.");
  }

  return db
    .collection("rounds")
    .doc(id)
    .delete();
}

async function createRound(round: Round.Round): Promise<Round.Round> {
  const newRoundRef = db.collection("rounds").doc();
  const { id, ...roundWithoutId } = round;
  await newRoundRef.set(roundWithoutId);

  return {
    ...roundWithoutId,
    id: newRoundRef.id
  };
}

export async function getRound(roundId: string): Promise<Round.Round> {
  const querySnapshot = await db
    .collection("rounds")
    .doc(roundId)
    .get();

  return mapDocToRound(querySnapshot);
}

export async function getUserRounds(uid: string): Promise<Round.Round[]> {
  const querySnapshot = await db
    .collection("rounds")
    .where("createdBy", "==", uid)
    .orderBy("started", "desc")
    .get();
  const roundData: Round.Round[] = [];
  querySnapshot.forEach(doc => roundData.push(mapDocToRound(doc)));
  return roundData;
}

function mapDocToRound(
  doc:
    | firebase.firestore.QueryDocumentSnapshot
    | firebase.firestore.DocumentSnapshot
): Round.Round {
  const data = doc.data();
  return Round.mapJsonToRound({
    id: doc.id,
    ...data
  });
}
