import { Target } from "../../model/target";
import { TargetScores } from "../../model/score";

export enum Merit {
  DOUBLE_KILL = "DOUBLE_KILL",
  TRIPLE_KILL = "TRIPLE_KILL",
  QUAD_KILL = "QUAD_KILL",
  MEGA_KILL = "MEGA_KILL",
  MONSTER_KILL = "MONSTER_KILL"
}

export type ArcherMerits = {
  [archerId: string]: { [merit: string]: number };
};

export type MeritsEngine = (
  targets: Target[],
  scores: TargetScores
) => ArcherMerits;
