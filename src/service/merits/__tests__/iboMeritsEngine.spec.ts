import { TargetScores } from "../../../model/score";
import { Target } from "../../../model/target";
import { Merit } from "../interface";
import iboMeritsEngine from "../iboMeritsEngine";

describe("Services :: Merits :: IBO Merits Engine", () => {
  let scores: TargetScores;
  let targets: Target[];

  beforeEach(() => {
    targets = [
      {
        id: "t1",
        name: "bear",
        yardage: 10
      },
      {
        id: "t2",
        name: "deer",
        yardage: 80
      },
      {
        id: "t3",
        name: "lion",
        yardage: 30
      }
    ];

    scores = {
      t1: {
        a1: 10,
        a2: 11,
        a3: 10
      },
      t2: {
        a1: 11,
        a2: 10,
        a3: 4
      },
      t3: {
        a1: 10,
        a2: 11,
        a3: 11
      }
    };
  });

  describe("DOUBLE_KILL", () => {
    it("should give a double kill merit for two inner ring scores", () => {
      const merits = iboMeritsEngine(targets, scores);
      expect(merits.a1[Merit.DOUBLE_KILL]).toBe(1);
      expect(merits.a2[Merit.DOUBLE_KILL]).toBe(1);
      expect(merits.a3[Merit.DOUBLE_KILL]).toBe(0);
    });
  });
});
