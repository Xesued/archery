import { ScoringType, TargetScores, ScoringMethod } from "../../model/score";
import { Target } from "../../model/target";

import { ArcherMerits } from "./interface";
import IBOMeritsEngine from "./iboMeritsEngine";

const scoringTypeToEngine = {
  [ScoringType.IBO]: IBOMeritsEngine,
  [ScoringType.ASA]: () => ({})
};

export default function engine(
  targets: Target[],
  scores: TargetScores,
  method: ScoringMethod
): ArcherMerits {
  return scoringTypeToEngine[method.type]
    ? scoringTypeToEngine[method.type](targets, scores)
    : {};
}
