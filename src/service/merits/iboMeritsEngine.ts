import { TargetScores } from "../../model/score";
import { Target } from "../../model/target";

import { MeritsEngine, Merit, ArcherMerits } from "./interface";

const iboMeritsEngine: MeritsEngine = (targets, scores) => {
  let merits: ArcherMerits = {};
  merits = addDoubleKillMerits(scores, merits);

  return merits;
};

function addDoubleKillMerits(
  scores: TargetScores,
  merits: ArcherMerits
): ArcherMerits {
  return merits;
}

function countAdjacentScores(scores: number[], threshold: number): number {
  let count = 0;
  scores.forEach((score, index) => {
    if (index > 0) {
      if (score >= threshold && scores[index - 1] >= threshold) {
        count += 1;
      }
    }
  });

  return count;
}

function addMerits(
  merits: ArcherMerits,
  archerId: string,
  merit: Merit,
  count: number
) {
  if (!merits[archerId]) {
    merits[archerId] = {};
  }

  if (!merits[archerId][merit]) {
    merits[archerId][merit] = 0;
  }
  merits[archerId][merit] += count;
}

/**
 * Builds out a map where the keys are the archer ids and the
 * values are an ordered list of their scores.
 */
function buildScoresByArcher(
  scores: TargetScores
): { [archerId: string]: number[] } {
  return {};
}

export default iboMeritsEngine;
