import { Round, mapJsonToRound } from "../model/round";

type RoundCallback = (rnd: Round | null) => void;
const callbacks: Array<RoundCallback> = [];

export function getDraftRound(): Round | null {
  const roundStr = window.localStorage.getItem("draftRound");
  if (roundStr) {
    try {
      const obj = JSON.parse(roundStr);
      return obj ? mapJsonToRound(obj) : null;
    } catch (e) {
      return null;
    }
  }
  return null;
}

export function saveDraftRound(round: Round | null) {
  window.localStorage.setItem("draftRound", JSON.stringify(round));
  callbacks.forEach(cb => cb(round));
}

export function onDraftRoundUpdate(callback: RoundCallback) {
  callbacks.push(callback);
  callback(getDraftRound());
}

export function offDraftRoundUpdate(callback: RoundCallback) {
  const foundCallbackIndex = callbacks.findIndex(cb => cb === callback);
  if (foundCallbackIndex) {
    delete callbacks[foundCallbackIndex];
  }
}
