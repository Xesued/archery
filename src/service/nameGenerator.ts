const adjectives = [
  "Skilled",
  "Legendary",
  "Victorious",
  "Unworthy",
  "Handsome",
  "Mighty",
  "Old",
  "Sleepy"
];

const nouns = [
  "Lynx",
  "Elk",
  "Deer",
  "Squirrel",
  "Lion",
  "Turkey",
  "Fish",
  "Fox"
];

function getRandomIndex(length: number): number {
  return Math.floor(Math.random() * Math.floor(length));
}

export default function randomName(): string {
  const adjectiveIndex = getRandomIndex(adjectives.length);
  const nounIndex = getRandomIndex(nouns.length);

  return `${adjectives[adjectiveIndex]} ${nouns[nounIndex]}`;
}
