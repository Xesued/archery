import db from "./service/firebase";
import auth from "./service/auth";
import * as DraftDB from "./service/draft";
import * as Round from "./model/round";

type ErrorData = {
  type: string;
  message: string;
  time: number;
  stack: string;
  archer: null | { id: string; name: string | null; email: string | null };
  draftRound: null | Round.Round;
};

window.addEventListener("error", event => {
  const errorData: ErrorData = {
    type: event.type,
    message: event.message,
    time: event.timeStamp,
    stack: event.error.stack,
    archer: null,
    draftRound: null
  };

  const newErrorRef = db.collection("errors").doc();

  try {
    const user = auth.getUserOrError();
    const archer = {
      id: user.uid,
      name: user.displayName,
      email: user.email
    };
    errorData.archer = archer;
  } catch (e) {
    // Do nothing
  }

  const round = DraftDB.getDraftRound();
  errorData.draftRound = round;

  newErrorRef.set(errorData);
});
