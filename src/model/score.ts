import cloneDeep from "lodash/cloneDeep";

import { Merit } from "../model/merit";

export type ArcherTargetScore = {
  score: number;
  merits: Merit[];
};

export type TargetScores = {
  [targetId: string]: {
    order: string[];
    scores: {
      [archerId: string]: ArcherTargetScore;
    };
  };
};

export enum ScoringType {
  IBO = "IBO",
  ASA = "ASA"
}

export interface ScoringMethod {
  values: number[];
  type: ScoringType;
}

export const IBOScoringMethod: ScoringMethod = {
  values: [0, 5, 8, 10, 11],
  type: ScoringType.IBO
};

export const ASAScoringMethod: ScoringMethod = {
  values: [0, 5, 8, 10, 12, 14],
  type: ScoringType.ASA
};

export function getScoringMethodFromType(
  type: ScoringType | string
): ScoringMethod {
  switch (type) {
    case ScoringType.IBO:
      return IBOScoringMethod;

    case ScoringType.ASA:
      return ASAScoringMethod;

    default:
      throw Error(`Unknowning scoring type: ${type}`);
  }
}

export function copyTargetScore(ts: TargetScores): TargetScores {
  return cloneDeep(ts);
}

export function updateArchersScore(
  ts: TargetScores,
  targetId: string,
  archerId: string,
  value: number
): TargetScores {
  return {
    ...ts,
    [targetId]: {
      ...ts[targetId],
      scores: {
        ...ts[targetId].scores,
        [archerId]: {
          ...ts[targetId].scores[archerId],
          score: value
        }
      }
    }
  };
}
