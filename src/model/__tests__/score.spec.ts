import * as scoring from "../score";

describe("Model -> Score", () => {
  describe("getScoringMethodFromType", () => {
    const typesToMethodsMap = {
      [scoring.ScoringType.IBO]: scoring.IBOScoringMethod,
      [scoring.ScoringType.ASA]: scoring.ASAScoringMethod
    };

    Object.entries(typesToMethodsMap).forEach(typeAndMethod => {
      it(`should return the correct scoring for an ${typeAndMethod[0]} type`, () => {
        const results = scoring.getScoringMethodFromType(typeAndMethod[0]);
        expect(results).toBe(typeAndMethod[1]);
      });
    });
  });
});
