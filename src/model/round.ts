import * as array from "../lib/array";

import { Archer } from "./archer";
import { Target } from "./target";
import { TargetScores, ScoringMethod, ArcherTargetScore } from "./score";

export interface RoundLocation {
  latitude: number;
  longitude: number;
  accuracy: number;
}

export interface Round {
  /**
   * Id of the round.  A null id means it's still a draft that hasn't
   * been saved.
   */
  id: string | null;

  /**
   * The id of the user that created the round
   */
  createdBy: string | null;

  /**
   * A list of archers that participated.
   */
  archers: Archer[];

  /**
   * A list of targets for the round.
   *
   * If this is an ad-hoc round, it starts with zero and
   * added in as you go.
   */
  targets: Target[];

  /**
   * A name that the user can identify the round with.
   */
  name: string;

  /**
   * When the round was started. (Unix timestamp)
   */
  started: number | null;

  /**
   * When the round was completed. (Unix timestamp)
   */
  finished: number | null;

  /**
   * Scoring method used to score the round.
   */
  scoringMethod: ScoringMethod;

  /**
   * The scores of the archers for the round.
   */
  targetScores: TargetScores;

  /**
   * The GEO location of where the round was started.
   */
  location: RoundLocation | null;
}

type ScoresByArcher = {
  [archerId: string]: number;
};

export function addArcher(round: Round, archer: Archer): Round {
  return {
    ...round,
    archers: [...round.archers, archer]
  };
}

export function updateArcher(round: Round, archer: Archer): Round {
  const archerIndex = round.archers.findIndex(a => a.id === archer.id);
  if (archerIndex > -1) {
    return {
      ...round,
      archers: [
        ...round.archers.slice(0, archerIndex),
        archer,
        ...round.archers.slice(archerIndex + 1)
      ]
    };
  }

  return round;
}

export function removeArcherByIndex(round: Round, archerIndex: number): Round {
  return {
    ...round,
    archers: [
      ...round.archers.slice(0, archerIndex),
      ...round.archers.slice(archerIndex + 1)
    ]
  };
}

/**
 * Gets all of the archers placements for a finsihed round.
 *
 * @param round
 */
export function getPlacement(round: Round): Array<[string, number]> {
  const archerScores = Object.keys(round.targetScores).reduce<ScoresByArcher>(
    (acc, targetId) => {
      const targetScores = round.targetScores[targetId];
      Object.keys(targetScores.scores).forEach(archerId => {
        if (!acc[archerId]) acc[archerId] = 0;
        acc[archerId] += targetScores.scores[archerId].score;
      });
      return acc;
    },
    {}
  );

  const entries = Object.entries(archerScores);
  entries.sort((a, b) => {
    if (a[1] > b[1]) return -1;
    if (a[1] < b[1]) return 1;
    return 0;
  });

  return entries;
}

/**
 * Adds a target to a round.  Also calculates the turn order and
 * sets up the targetScores for this target.
 */
export function addTarget(
  round: Round,
  target: Target,
  order: string[]
): Round {
  return {
    ...round,
    targets: [...round.targets, target],
    targetScores: {
      ...round.targetScores,
      [target.id]: {
        order,
        scores: {}
      }
    }
  };
}

/**
 * Updates a target in a round.
 *
 * @param round
 * @param target
 */
export function updateTarget(round: Round, target: Target): Round {
  const targetIndex = round.targets.findIndex(t => t.id === target.id);
  if (targetIndex !== -1) {
    return {
      ...round,
      targets: [
        ...round.targets.slice(0, targetIndex),
        target,
        ...round.targets.slice(targetIndex + 1)
      ]
    };
  }

  return round;
}

export function removeTarget(round: Round, targetId: string): Round {
  const targetIndex = round.targets.findIndex(t => t.id === targetId);
  if (targetIndex !== -1) {
    const newTargets = array.remove(round.targets, targetIndex);
    return {
      ...round,
      targets: newTargets
    };
  }
  return round;
}

/**
 * Sets the scores for the targes in a round.
 *
 * @param round
 * @param target
 */
export function updateTargetScores(round: Round, ts: TargetScores): Round {
  return {
    ...round,
    targetScores: ts
  };
}

/**
 * Adds or updates a score for an archer.
 *
 * @param round
 * @param target
 * @param archerId
 * @param archerTargetScore
 */
export function addScore(
  round: Round,
  target: Target,
  archerId: string,
  archerTargetScore: ArcherTargetScore
): Round {
  return {
    ...round,
    targetScores: {
      ...round.targetScores,
      [target.id]: {
        ...round.targetScores[target.id],
        scores: {
          ...round.targetScores[target.id].scores,
          [archerId]: archerTargetScore
        }
      }
    }
  };
}

/**
 * Given a round and target, we calculate what the order
 * is for the target.  It rotates from target to target.
 *
 * @param round
 * @param targetIndex
 */
export function getArcherOrder(round: Round, targetIndex?: number) {
  const targetsLength = targetIndex || round.targets.length;
  const archersLength = round.archers.length;

  const startIndex = targetsLength % archersLength;

  return [
    ...round.archers.slice(startIndex),
    ...round.archers.slice(0, startIndex)
  ];
}

/**
 * Creates a new round object, simply using the
 * defaults given.
 *
 * @param name
 * @param scoringMethod
 * @param targets
 * @param archers
 */
export function createRound(
  name: string,
  userId: string,
  scoringMethod: ScoringMethod,
  targets: Target[],
  archers: Archer[] = [],
  location: RoundLocation | null
): Round {
  return {
    id: null,
    createdBy: userId,
    archers,
    targets,
    name,
    started: Date.now(),
    targetScores: {},
    finished: null,
    scoringMethod,
    location
  };
}

/**
 * Builds out all of the turn orders for a target if it
 * hasn't been done already.
 *
 * @param round
 */
export function initTargetScores(round: Round, targetId: string) {
  const targetPos = round.targets.findIndex(t => t.id === targetId);
  if (targetPos > -1) {
    const order = getArcherOrder(round, targetPos);
    if (round && round.targetScores && round.targetScores[targetId]) {
      // Already initalized.
      return round;
    }

    return {
      ...round,
      targetScores: {
        ...round.targetScores,
        [targetId]: {
          order: order.map(o => o.id),
          scores: {}
        }
      }
    };
  }
  return round;
}

/**
 * Find and return the target that is after the one with
 * the give id.
 *
 * Returns null if it was the last round.
 *
 * @param round
 * @param targetId
 */
export function nextTarget(round: Round, targetId: string): Target | null {
  const targetPos = round.targets.findIndex(t => t.id === targetId);
  if (targetPos > -1 && targetPos + 1 < round.targets.length) {
    return round.targets[targetPos + 1];
  }
  return null;
}

/**
 * Sets the scoring method that will be used for the round.
 *
 * @param round
 * @param method
 */
export function setScoringMethod(round: Round, method: ScoringMethod) {
  return {
    ...round,
    scoringMethod: method
  };
}

/**
 * normalizes the score for an archer.  Returns
 * 0 if the archer does not have a score .
 */
function getArcherScore(
  archerScores: { [as: string]: ArcherTargetScore },
  archerId: string
): number {
  const score = archerScores[archerId];
  return score ? score.score : 0;
}

/**
 * Gets the rounds total score for an
 * archer.
 *
 */
export function getRoundScoreForArcher(round: Round, archerId: string): number {
  let score = 0;
  Object.values(round.targetScores).forEach(
    targetScore => (score += getArcherScore(targetScore.scores, archerId))
  );
  return score;
}

export function getRoundAvgYardage(round: Round) {
  const targetCountWithYardage = round.targets.reduce(
    (acc, t) => acc + (t.yardage ? 1 : 0),
    0
  );
  const targetYardageTotal = round.targets.reduce(
    (acc, t) => acc + (t.yardage ? parseInt("" + t.yardage, 10) : 0),
    0
  );

  return Math.round((targetYardageTotal / targetCountWithYardage) * 100) / 100;
}

/**
 * Gets the average score shot on all targets in
 * the round.
 *
 */
export function getRoundTargetAvgScore(round: Round) {
  const targetTotalScore = Object.values(round.targetScores).reduce(
    (acc, archerScores) => {
      return (
        acc +
        Object.values(archerScores.scores).reduce(
          (asAcc, as) => asAcc + as.score,
          0
        )
      );
    },
    0
  );
  const targetShots = Object.values(round.targetScores).reduce(
    (acc, archerScores) => {
      return acc + Object.values(archerScores).length;
    },
    0
  );

  return Math.round((targetTotalScore / targetShots) * 100) / 100;
}

export function getRoundMisses(round: Round) {
  return Object.values(round.targetScores).reduce((acc, archerScores) => {
    return (
      acc +
      Object.values(archerScores.scores).reduce(
        (asAcc, as) => asAcc + (as.score === 0 ? 1 : 0),
        0
      )
    );
  }, 0);
}

/**
 * Totals an archers score for the whole
 * round.
 */
export function getArcherTargetScores(
  round: Round,
  archer: Archer
): Array<{ name: string; score: number; yardage: number }> {
  const targets: Array<{ name: string; score: number; yardage: number }> = [];
  round.targets.forEach(t => {
    const targetScore = round.targetScores[t.id];
    if (targetScore) {
      const archerScore = targetScore.scores[archer.id];
      if (archerScore) {
        // We could have a target created where no scores were added to it.
        targets.push({
          name: t.name,
          score: archerScore.score,
          yardage: t.yardage || 0
        });
      }
    }
  });

  return targets;
}

export function mapJsonToRound(data: any): Round {
  return {
    id: data.id,
    createdBy: data.createdBy,
    name: data.name,
    archers: data.archers,
    finished: data.finished,
    scoringMethod: data.scoringMethod,
    started: data.started,
    targetScores: data.targetScores,
    targets: data.targets,
    location: data.location
  };
}
