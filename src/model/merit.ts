export enum Merit {
  DOUBLE_KILL = "DOUBLE_KILL",
  TRIPLE_KILL = "TRIPLE_KILL",
  QUAD_KILL = "QUAD_KILL",
  MEGA_KILL = "MEGA_KILL",
  MONSTER_KILL = "MONSTER_KILL"
}
