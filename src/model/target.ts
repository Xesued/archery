export interface Target {
  id: string;
  name: string;
  image: string | null;
  yardage?: number;
}

export function copy(t: Target): Target {
  return {
    ...t
  };
}

export function update(t: Target, partial: Partial<Target>): Target {
  return {
    ...t,
    ...partial
  };
}
