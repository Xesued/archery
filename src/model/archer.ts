import { Round } from "./round";

export type Archer = {
  id: string;
  name: string;
  imageIndex: number;
};

export function getNextArcherId(
  round: Round,
  targetId: string,
  archerId?: string
): string | null {
  const targetTurnOrder = round.targetScores[targetId].order;

  if (!archerId) {
    // We are not on any archer right now... take from the top.
    return targetTurnOrder[0];
  }

  const archerIndex = targetTurnOrder.findIndex(a => a === archerId);
  if (archerIndex !== -1 && round.archers.length - 1 > archerIndex) {
    return targetTurnOrder[archerIndex + 1];
  }

  return null;
}
