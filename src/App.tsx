import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";

import { ThemeProvider } from "@material-ui/core/styles";

import "./service/draft";

import vitalShotTheme from "./theme";

import AppHeader from "./components/AppHeader";
import PrivateRoute from "./components/PrivateRoute";
import UpdateSnackBar from "./components/UpdateSnackbar";

import Home from "./views/Home";
import RoundHome from "./views/round";

import Login from "./views/Login";
import AppMenu from "./AppMenu";

import "typeface-roboto";

import "./errorLogger";

type AppProps = {
  reg: ServiceWorkerRegistration;
};

function App(props: AppProps) {
  const { reg } = props;
  const [isDrawerOpen, setDrawerOpen] = useState(false);

  function onSkipWaiting() {
    if (reg && reg.waiting) {
      reg.waiting?.postMessage({ type: "SKIP_WAITING" });
    }

    if (reg && reg.installing) {
      reg.waiting?.postMessage({ type: "SKIP_WAITING" });
    }

    let refreshing = false;
    navigator.serviceWorker.addEventListener("controllerchange", function() {
      if (refreshing) return;
      refreshing = true;
      window.location.reload();
    });
  }

  return (
    <ThemeProvider theme={vitalShotTheme}>
      <Router>
        <CssBaseline />
        <AppMenu isOpen={isDrawerOpen} onClose={() => setDrawerOpen(false)} />
        <AppHeader onOpenDrawer={() => setDrawerOpen(true)} />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/login" exact component={Login} />
          <PrivateRoute path="/round" component={RoundHome} />
        </Switch>
        <UpdateSnackBar hasUpdate={!!reg} onSkipWaiting={onSkipWaiting} />
      </Router>
    </ThemeProvider>
  );
}

export default App;
