import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";

import HomeIcon from "@material-ui/icons/Home";
import AddBoxIcon from "@material-ui/icons/AddBox";
import TimelineIcon from "@material-ui/icons/Timeline";

import ListItemLink from "./components/ListItemLink";

import * as Round from "./model/round";
import * as DraftDb from "./service/draft";

import { ReactComponent as VitalShotLogo } from "./vital-shot-logo.svg";

const useStyles = makeStyles(theme => ({
  list: {
    width: 216
  },
  icon: {
    marginRight: theme.spacing(2),
    width: 32
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6)
  }
}));

type AppMenuProps = {
  onClose: () => void;
  isOpen: boolean;
};

export default function AppMenu(props: AppMenuProps) {
  const { onClose, isOpen } = props;
  const classes = useStyles();
  const [draftRound, setDraftRound] = useState<Round.Round | null>(null);
  const history = useHistory();

  useEffect(() => {
    const onDraftRoundUpdate: (rnd: Round.Round | null) => void = round => {
      setDraftRound(round);
    };

    DraftDb.onDraftRoundUpdate(onDraftRoundUpdate);
    return () => DraftDb.offDraftRoundUpdate(onDraftRoundUpdate);
  }, []);

  function resumeRound() {
    onClose();
    if (!draftRound) return;

    const lastTargetIndex = draftRound.targets.length - 1;
    if (lastTargetIndex < 0) {
      // No targets yet. Build first target.
      history.push({
        pathname: "/round/draft/target/new"
      });
      return;
    }

    const lastTarget = draftRound.targets[lastTargetIndex];
    const numOfArchers = draftRound.archers.length;
    const targetScores = draftRound.targetScores[lastTarget.id];
    const archerScores = targetScores.scores;
    const numOfArchersScored = Object.keys(archerScores).length;

    if (numOfArchersScored === numOfArchers) {
      // All of the archers have been scored, navigate creating a new target.
      history.push({
        pathname: "/round/draft/target/new"
      });
      return;
    }

    // Some or none of the archers have been scored.  Find the first archer that needs
    // score.
    const { order } = targetScores;
    const firstNonScoredArcherId = order.find(
      archerId => !archerScores[archerId]
    );
    history.push({
      pathname: `/round/draft/target/${lastTarget.id}/archer/${firstNonScoredArcherId}`
    });
  }

  return (
    <Drawer open={isOpen} onClose={onClose}>
      <List className={classes.list}>
        <ListItem>
          <ListItemIcon>
            <VitalShotLogo className={classes.icon} />
          </ListItemIcon>
          <ListItemText primary="Vital Shot" />
        </ListItem>
      </List>
      <Divider />
      <List className={classes.list} component="nav">
        <ListItemLink
          onClick={onClose}
          to="/"
          primary="Home"
          icon={<HomeIcon />}
        />
        <ListItemLink
          onClick={onClose}
          to="/round/draft"
          primary="New Round"
          icon={<AddBoxIcon />}
        />
        {draftRound && (
          <ListItem onClick={() => resumeRound()}>
            <ListItemIcon>
              <AddBoxIcon color="secondary" />
            </ListItemIcon>
            <ListItemText
              primary="Resume Round"
              secondary="A round is in progress"
            />
          </ListItem>
        )}
        <ListItemLink
          onClick={onClose}
          to="/round"
          primary="Past Rounds"
          icon={<TimelineIcon />}
        />
      </List>
    </Drawer>
  );
}
