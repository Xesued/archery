import { useState, useEffect } from "react";
import * as firebase from "firebase";
import auth from "../service/auth";

export default function useAuth(): firebase.User | null | Error {
  const [needToAuth, updateNeedToAuth] = useState(true);
  const [user, updateUser] = useState<firebase.User | null | Error>(null);

  useEffect(() => {
    if (needToAuth) {
      updateNeedToAuth(false);
      auth.isAuthenticated().then(isAuthenticated => {
        if (!isAuthenticated) {
          updateUser(Error("User not valid"));
        } else {
          updateUser(auth.user);
        }
      });
    }
  }, [needToAuth]);

  return user;
}
