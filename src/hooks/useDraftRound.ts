import { useState, useEffect } from "react";
import * as DraftDB from "../service/draft";
import * as Round from "../model/round";

export default function useDraftRound(): [
  Round.Round | null,
  (r: Round.Round) => void
] {
  const [round, updateLocalRound] = useState<Round.Round | null>(null);

  useEffect(() => {
    const draftRound = DraftDB.getDraftRound();
    updateLocalRound(draftRound);

    function syncRound(newRound: Round.Round | null) {
      updateLocalRound(newRound);
    }

    DraftDB.onDraftRoundUpdate(syncRound);
    return () => {
      DraftDB.offDraftRoundUpdate(syncRound);
    };
  }, []);

  function updateRound(round: Round.Round) {
    DraftDB.saveDraftRound(round);
    updateLocalRound(round);
  }

  return [round, updateRound];
}
