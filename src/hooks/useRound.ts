import { useState, useEffect } from "react";
import * as FirebaseDB from "../service/db/rounds";
import { Round } from "../model/round";

export default function useRound(
  roundId: string
): [Round | null, (r: Round) => void] {
  const [round, setRound] = useState<Round | null>(null);

  useEffect(() => {
    const getRound = async () => {
      const draftRound = await FirebaseDB.getRound(roundId);
      setRound(draftRound);
    };

    getRound();
  }, [roundId]);

  const updateRound = async (round: Round) => {
    const draftRound = await FirebaseDB.saveRound(round);
    setRound(draftRound);
  };

  return [round, updateRound];
}
