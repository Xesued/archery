import { useState, useEffect } from "react";

type Coordinates = {
  latitude: number;
  longitude: number;
  accuracy: number;
};

export default function useGeoLocation(): Coordinates | null {
  const [coordinates, setCoordinates] = useState<Coordinates | null>(null);
  useEffect(() => {
    const success: PositionCallback = c => {
      const { latitude, longitude, accuracy } = c.coords;
      setCoordinates({
        latitude,
        longitude,
        accuracy
      });
    };

    const error = (e: any) => {
      console.log("Could not get possition", e);
    };

    navigator.geolocation.getCurrentPosition(success, error);
  }, []);

  return coordinates;
}
