import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { ensureOnce } from "./helpers";

admin.initializeApp();

const db = admin.firestore();

export const updateCourseStats = functions.firestore
  .document("rounds/{roundId}")
  .onCreate(async (snap, context) => {
    // do stuff
    const roundData = snap.data();
    if (!roundData) {
      console.log("__ERROR__");
      return;
    }

    try {
      await ensureOnce(context.eventId);
      const courseRef = db.collection("courses").doc(roundData.courseId);
      const courseSnap = await courseRef.get();
      const courseData = courseSnap.data();
      if (courseData) {
        const newStats = courseData && courseData.stats ? courseData.stats : {};
        newStats.roundsShot =
          newStats.roundsShot !== undefined ? newStats.roundsShot + 1 : 1;
        console.log("updated stats", newStats);
        await courseRef.update({ stats: newStats });
      }
    } catch (e) {
      console.warn("Did not update course", e);
    }
  });
