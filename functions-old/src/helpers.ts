import * as admin from "firebase-admin";

/**
 * Helper function to make sure we only process an event once.
 *
 * This returns a promise that is rejected if we've already processed
 * the event or if the transaction failed on the realtime database.
 *
 * @param eventId Id of a firestore event
 */
export async function ensureOnce(eventId: string): Promise<any> {
  const db = admin.database();
  const eventRef = db.ref(`firestoreEvents/${eventId}`);
  return new Promise((resolve, reject) => {
    // tslint:disable-next-line: no-floating-promises
    eventRef.transaction(
      value => {
        if (value === null) {
          return true;
        }
        return; // Return undefined to cancel transaction
      },
      (error, committed) => {
        if (error) {
          console.log("Event transaction failed...", error);
          reject("Transaction Failed");
        } else if (!committed) {
          console.log("Event procesed once already");
          reject("Event already processed");
        } else {
          console.log("Event processed successfully");
          resolve();
        }
      }
    );
  });
}
